import './App.css'

import React, { Component } from 'react'
import TopPage from './components/TopPage'
import { Route } from 'react-router-dom'
import { Switch } from 'react-router'
import Complete from './components/Complete'
import PizzaComponent from './components/Pizza/PizzaComponent'
import NavBar from './components/NavBar/NavBar'
class App extends Component {
  render() {
    return (
      <div>
        <NavBar />
        <Switch>
          <Route path="/" exact component={TopPage} />
          <Route path="/pizza" exact component={PizzaComponent} />
          <Route path="/complete" exact component={Complete} />
        </Switch>
      </div>
    )
  }
}

export default App
