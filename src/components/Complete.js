import React from 'react'

const Complete = () => {
  return <h1 className="page-content">Thank you for your order!</h1>
}

export default Complete
