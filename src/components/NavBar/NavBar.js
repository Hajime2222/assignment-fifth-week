import { AppBar, Toolbar, Typography } from '@material-ui/core'
import React from 'react'
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core'
import { Button } from '@material-ui/core'

const styles = {
  logo: {
    flexGrow: 1,
    textAlign: 'left'
  }
}
const NavBar = props => {
  return (
    <AppBar position="static" color="default">
      <Toolbar>
        <Typography
          variant="title"
          color="inherit"
          className={props.classes.logo}
        >
          <Link to="/">NewAgePizza.com</Link>
        </Typography>
        <Link to="/">
          <Button>Top</Button>
        </Link>
        <Link to="/pizza">
          <Button>Pizza</Button>
        </Link>
      </Toolbar>
    </AppBar>
  )
}

export default withStyles(styles)(NavBar)
