import React from 'react'
import { List, ListItem, ListItemText } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import formatPrice from '../../lib/formatPrice'
import { Divider } from '@material-ui/core'
const styles = {
  root: {
    backgroundColor: 'pink'
  }
}

const PizzaSause = props => {
  return (
    <div className="page-content">
      <p>Choose your sause</p>
      <Divider />
      <List component="nav">
        {props.ingredients &&
          props.ingredients.map(i => {
            return (
              <ListItem
                button
                key={i.id}
                onClick={() => props.pickSause(i)}
                className={
                  props.selectedSause && props.selectedSause.id === i.id
                    ? props.classes.root
                    : ''
                }
                style={{ textAlign: 'center' }}
              >
                <ListItemText primary={`${i.name}: €${formatPrice(i.price)}`} />
              </ListItem>
            )
          })}
      </List>
    </div>
  )
}

export default withStyles(styles)(PizzaSause)
