import {
  Card,
  CardContent,
  Checkbox,
  Divider,
  Grid,
  Typography,
  withStyles
} from '@material-ui/core'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import React from 'react'
import formatPrice from '../../lib/formatPrice'
import TotalPrice from './TotalPrice'

const styles = {
  card: {
    margin: '2em',
    width: '90%',
    maxWidth: '800px'
  },
  divider: {
    margin: '1em 0'
  }
}

const Checkout = props => {
  const { pizza, classes } = props
  return (
    <div className="page-content">
      <Typography variant="headline" component="h1">
        Checkout?
      </Typography>
      <Divider />
      {Object.entries(pizza).map(([key, val]) => {
        return (
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            key={key}
          >
            <Card className={classes.card}>
              <CardContent>
                <Typography
                  variant="headline"
                  style={{ textTransform: 'capitalize' }}
                >
                  {key}
                </Typography>
                <Divider className={classes.divider} />

                {Array.isArray(val) ? (
                  val.map(v => {
                    return (
                      <Typography variant="headline" component="h2" key={v.id}>
                        {v.name}: €{formatPrice(v.price)}
                      </Typography>
                    )
                  })
                ) : (
                  <Typography variant="headline" component="h2">
                    {val.name}: €{formatPrice(val.price)}
                  </Typography>
                )}
              </CardContent>
            </Card>
          </Grid>
        )
      })}

      <FormControlLabel
        control={
          <Checkbox
            checked={props.turbo}
            onChange={props.toggleTurbo}
            color="primary"
          />
        }
        label=" turbo-drone-delivery (+10%)"
      />

      <TotalPrice />
    </div>
  )
}

export default withStyles(styles)(Checkout)
