import React, { Component } from 'react'
import Checkout from './Checkout'
import { connect } from 'react-redux'
import { toggleTurboOption } from '../../store/actions/turbo'
import { resetState } from '../../store/actions/commonActions'

class CheckoutContainer extends Component {
  toggleTurbo = () => {
    this.props.toggleTurboOption()
  }
  render() {
    return (
      <Checkout
        pizza={this.props.pizza}
        turbo={this.props.turbo}
        toggleTurbo={this.toggleTurbo}
        resetState={this.props.resetState}
      />
    )
  }
}
const mapStateToProps = state => ({
  turbo: state.turboOption,
  pizza: state.pizza
})

export default connect(
  mapStateToProps,
  { toggleTurboOption, resetState }
)(CheckoutContainer)
