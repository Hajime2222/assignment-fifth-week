import React, { Component } from 'react'
import PizzaToppings from './PizzaToppings'
import { addIngredient } from '../../store/actions/pizza'
import { connect } from 'react-redux'

class PizzaToppingsContainer extends Component {
  pickToppings = topping => {
    const prevToppings = [...this.props.pizza.toppings]
    const isIncluded = prevToppings.some(t => t.id === topping.id)

    const updatedTopping = !isIncluded
      ? [...prevToppings, topping]
      : prevToppings.filter(t => t.id !== topping.id)
    this.props.addIngredient('toppings', updatedTopping)
  }
  render() {
    return (
      <PizzaToppings
        ingredients={this.props.toppings}
        pickToppings={this.pickToppings}
        selectedToppings={this.props.pizza.toppings}
      />
    )
  }
}
const mapStateToProps = state => ({
  toppings: state.ingredients.toppings,
  pizza: state.pizza
})

export default connect(
  mapStateToProps,
  { addIngredient }
)(PizzaToppingsContainer)
