import React from 'react'
import { connect } from 'react-redux'
import formatPrice from '../../lib/formatPrice'

const TotalPrice = props => {
  const { pizza } = props

  let totalPrice = Object.values(pizza).reduce((total, ingredient) => {
    if (!ingredient) return total
    if (Array.isArray(ingredient)) {
      const toppingTotal = ingredient.reduce(
        (tTotal, t) => (tTotal += t.price),
        0
      )
      total = toppingTotal ? total + toppingTotal : total
    } else {
      total += ingredient.price
    }
    return total
  }, 0)

  if (props.turbo) totalPrice *= 1.1
  const finalPrice = formatPrice(totalPrice)

  return (
    <div>
      <h1>Total Price: {finalPrice}</h1>
    </div>
  )
}

const mapStateToProps = state => ({
  turbo: state.turboOption,
  pizza: state.pizza
})

export default connect(mapStateToProps)(TotalPrice)
