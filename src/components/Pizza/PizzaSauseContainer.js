import React, { Component } from 'react'
import { connect } from 'react-redux'
import PizzaSause from './PizzaSause'
import { addIngredient } from '../../store/actions/pizza'

class PizzaSauseContainer extends Component {
  pickSause = sause => {
    this.props.addIngredient('sause', sause)
  }
  render() {
    return (
      <PizzaSause
        ingredients={this.props.sause}
        pickSause={this.pickSause}
        selectedSause={this.props.selectedSause}
      />
    )
  }
}
const mapStateToProps = state => ({
  sause: state.ingredients.sause,
  selectedSause: state.pizza.sause
})

export default connect(
  mapStateToProps,
  { addIngredient }
)(PizzaSauseContainer)
