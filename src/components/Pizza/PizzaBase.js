import React from 'react'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import formatPrice from '../../lib/formatPrice'
import { Divider } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
const styles = {
  root: {
    backgroundColor: 'pink'
  }
}
const PizzaBase = props => {
  return (
    <div className="page-content">
      <p>Choose your base</p>
      <Divider />
      <List component="nav">
        {props.ingredients.map(i => {
          return (
            <ListItem
              button
              key={i.id}
              onClick={() => props.pickBase(i)}
              className={
                props.selectedBase && props.selectedBase.id === i.id
                  ? props.classes.root
                  : ''
              }
              style={{ textAlign: 'center' }}
            >
              <ListItemText primary={`${i.name}: €${formatPrice(i.price)}`} />
            </ListItem>
          )
        })}
      </List>
    </div>
  )
}

export default withStyles(styles)(PizzaBase)
