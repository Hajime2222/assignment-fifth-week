import { Button, withStyles } from '@material-ui/core'
import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { updatePageNum } from '../../store/actions/pageNum'
import CheckoutContainer from './CheckoutContainer'
import PizzaBaseContainer from './PizzaBaseContainer'
import PizzaSauseContainer from './PizzaSauseContainer'
import PizzaToppingsContainer from './PizzaToppingsContainer'
import TotalPrice from './TotalPrice'

const styles = {
  navButtons: {
    margin: '2em'
  },
  padding: {
    paddingBottom: '10%'
  }
}

const PizzaComponent = props => {
  const { classes, pageNum } = props
  const Components = [
    PizzaBaseContainer,
    PizzaSauseContainer,
    PizzaToppingsContainer,
    CheckoutContainer
  ]

  const currentSection = Object.keys(props.pizza)[pageNum] || null
  const isSelected = props.pizza[currentSection]

  const Component = Components[pageNum]

  return (
    <div>
      {pageNum !== Components.length - 1 ? <TotalPrice /> : null}

      <Component />

      <div className={classes.navButtons}>
        {pageNum > 0 ? (
          <Button
            color="primary"
            variant="outlined"
            onClick={() => props.updatePageNum('decrement')}
          >
            Back
          </Button>
        ) : null}

        {pageNum < Components.length - 1 ? (
          <Button
            color="primary"
            variant="outlined"
            onClick={() => props.updatePageNum('increment')}
            disabled={
              !isSelected ||
              (props.pizza.toppings.length > 3 && currentSection === 'toppings')
            }
          >
            Next
          </Button>
        ) : null}
      </div>

      {pageNum === Components.length - 1 ? (
        <div className={classes.padding}>
          <Link to="/complete">
            <Button
              variant="contained"
              color="primary"
              onClick={props.resetState}
            >
              Checkout
            </Button>
          </Link>
        </div>
      ) : null}
    </div>
  )
}

const mapStateToProps = state => ({
  pizza: state.pizza,
  pageNum: state.pizzaPageNum
})
export default connect(
  mapStateToProps,
  { updatePageNum }
)(withStyles(styles)(PizzaComponent))
