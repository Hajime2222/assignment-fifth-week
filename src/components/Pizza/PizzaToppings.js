import React from 'react'
import { List, ListItem, ListItemText } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import formatPrice from '../../lib/formatPrice'
import { Divider } from '@material-ui/core'
const styles = {
  root: {
    backgroundColor: 'pink'
  }
}
const PizzaToppings = props => {
  const selectedSauseIds = props.selectedToppings.map(t => t.id)

  return (
    <div className="page-content">
      <List component="nav">
        <p>Choose your toppings (up to 3)</p>
        <Divider />
        {props.ingredients &&
          props.ingredients.map(i => {
            const text = `${i.name}: €${formatPrice(i.price)}`
            return (
              <ListItem
                button
                key={i.id}
                onClick={() => props.pickToppings(i)}
                className={
                  selectedSauseIds && selectedSauseIds.includes(i.id)
                    ? props.classes.root
                    : ''
                }
                style={{ textAlign: 'center' }}
              >
                <ListItemText primary={text} />
              </ListItem>
            )
          })}
      </List>
    </div>
  )
}

export default withStyles(styles)(PizzaToppings)
