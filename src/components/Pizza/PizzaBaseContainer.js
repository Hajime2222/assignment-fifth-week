import React, { Component } from 'react'
import PizzaBase from './PizzaBase'
import { connect } from 'react-redux'
import { addIngredient } from '../../store/actions/pizza'

class PizzaBaseContainer extends Component {
  // state = {
  //   base: null
  // }
  pickBase = ingredient => {
    this.props.addIngredient('base', ingredient)
  }

  render() {
    return (
      <PizzaBase
        ingredients={this.props.base}
        pickBase={this.pickBase}
        selectedBase={this.props.selectedBase}
      />
    )
  }
}
const mapStateToProps = state => ({
  base: state.ingredients.base,
  selectedBase: state.pizza.base
})
export default connect(
  mapStateToProps,
  { addIngredient }
)(PizzaBaseContainer)
