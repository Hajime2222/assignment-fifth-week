import React from 'react'
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core'
import { Button } from '@material-ui/core'

const styles = {
  main: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    height: '100vh'
  },
  button: {
    maxWidth: '200px',
    margin: '0 auto'
  }
}

const TopPage = props => {
  return (
    <div className={props.classes.main}>
      <h1>Welcome to NewAgePizza.com</h1>
      <Link to="/pizza">
        <Button
          variant="contained"
          color="primary"
          className={props.classes.button}
        >
          choose your pizza
        </Button>
      </Link>
    </div>
  )
}

export default withStyles(styles)(TopPage)
