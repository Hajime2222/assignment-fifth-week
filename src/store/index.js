import { createStore, combineReducers } from 'redux'

import ingredients from './reducers/ingredients'

import pizza from './reducers/pizza'
import turboOption from './reducers/turboOption'
import pizzaPageNum from './reducers/pageNum'

const reducers = combineReducers({
  ingredients,
  turboOption,
  pizza,
  pizzaPageNum
})

const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default store
