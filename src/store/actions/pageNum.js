import {
  INCREMENT_PAGE_NUM,
  DECREMENT_PAGE_NUM
} from '../constants/actionTypes'

export const updatePageNum = type => {
  if (type === 'increment') return { type: INCREMENT_PAGE_NUM }
  else return { type: DECREMENT_PAGE_NUM }
}
