import { TOGGLE_TURBO_OPTION } from '../constants/actionTypes'

export const toggleTurboOption = () => ({ type: TOGGLE_TURBO_OPTION })
