import { RESET_STATE } from '../constants/actionTypes'

export const resetState = () => ({ type: RESET_STATE })
