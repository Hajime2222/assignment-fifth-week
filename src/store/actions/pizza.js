import { ADD_INGREDIENT } from '../constants/actionTypes'

export const addIngredient = (section, ingredient) => ({
  type: ADD_INGREDIENT,
  payload: {
    section,
    ingredient
  }
})
