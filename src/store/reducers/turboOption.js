import { RESET_STATE, TOGGLE_TURBO_OPTION } from '../constants/actionTypes'

export default (state = false, { type } = {}) => {
  switch (type) {
    case TOGGLE_TURBO_OPTION:
      return !state
    case RESET_STATE:
      return false
    default:
      return state
  }
}
