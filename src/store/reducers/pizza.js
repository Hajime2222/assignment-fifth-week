import { ADD_INGREDIENT, RESET_STATE } from '../constants/actionTypes'
const initialState = {
  base: null,
  sause: null,
  toppings: []
}

export default (state = initialState, { type, payload } = {}) => {
  switch (type) {
    case ADD_INGREDIENT:
      return {
        ...state,
        toppings: [...state.toppings],
        [payload.section]: payload.ingredient
      }
    case RESET_STATE:
      return initialState
    default:
      return state
  }
}
