import {
  INCREMENT_PAGE_NUM,
  DECREMENT_PAGE_NUM,
  RESET_STATE
} from '../constants/actionTypes'
export default (state = 0, { type } = {}) => {
  switch (type) {
    case INCREMENT_PAGE_NUM:
      return state + 1
    case DECREMENT_PAGE_NUM:
      return state - 1
    case RESET_STATE:
      return 0
    default:
      return state
  }
}
