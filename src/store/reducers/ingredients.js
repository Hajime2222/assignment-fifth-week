export default () => {
  return {
    base: [
      { id: 1, name: '25cm NY Style', price: 8.99 },
      { id: 2, name: '30cm NY Style', price: 11.49 },
      { id: 3, name: '35cm NY Style', price: 13.49 }
    ],
    sause: [
      { id: 1, name: 'White Sause', price: 0.6 },
      { id: 2, name: 'Red Sauce', price: 1 },
      { id: 3, name: 'Mix', price: 1.5 }
    ],
    toppings: [
      { id: 1, name: 'Pineapple', price: 0.5 },
      { id: 2, name: 'Corn', price: 0.5 },
      { id: 3, name: 'Olives (Green)', price: 0.5 },
      { id: 4, name: 'Red Onion', price: 0.5 },
      { id: 5, name: 'Spinach', price: 0.5 },
      { id: 6, name: 'Cherry Tomatos', price: 0.5 }
    ]
  }
}
