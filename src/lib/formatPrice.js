export default number => {
  const str = number.toFixed(2).toString()
  const oneDecimal = /(\.\d)$/
  const noDecimal = /\./

  if (str.match(oneDecimal)) {
    return str.replace(oneDecimal, '$10')
  } else if (!str.match(noDecimal)) {
    return str + '.00'
  }
  return str
}
